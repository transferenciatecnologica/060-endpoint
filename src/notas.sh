

# Peticiones a Rasa:
curl -X POST http://localhost:5005/model/parse -d '{"text": "He cambiado el número de teléfono de clave permanente y ahora no me deja entrar"}' -H {'Content-Type': 'application/json'} | python3 -m json.tool
curl -XPOST http://localhost:5005/webhooks/rest/webhook -d '{"sender":"id", "message": "He cambiado el número de teléfono de clave permanente y ahora no me deja entrar"}' -H "Content-type: application/json" | python3 -m json.tool

# Peticiones endpoint

curl -X POST http://localhost:5000/
curl -X GET http://localhost:5000/status | python3 -m json.tool
curl -XPOST http://localhost:5000/talk -d '{"sender":"id", "message":"He cambiado el número de teléfono de clave permanente y ahora no me deja entrar"}' -H "Content-Type: application/json" | python3 -m json.tool



{
    "intent": {
        "name": "clave_num_telefono",
        "confidence": 0.9992609024047852
    },
    "entities": [],
    "intent_ranking": [
        {
            "name": "clave_num_telefono",
            "confidence": 0.9992609024047852
        },
        {
            "name": "utilidad_respuesta_yes_otra_consulta_no",
            "confidence": 0.00020277655858080834
        },
        {
            "name": "utilidad_respuesta_no",
            "confidence": 0.00016653940838295966
        },
        {
            "name": "certificado_digital_info",
            "confidence": 0.0001157717706519179
        },
        {
            "name": "probar_finales",
            "confidence": 8.217218419304118e-05
        },
        {
            "name": "rea_problema",
            "confidence": 6.0691341786878183e-05
        },
        {
            "name": "Default_Fallback_Intent_fallback",
            "confidence": 5.6460117775714025e-05
        },
        {
            "name": "certificado_digital",
            "confidence": 2.463839882693719e-05
        },
        {
            "name": "certificado_problema",
            "confidence": 1.8294918845640495e-05
        },
        {
            "name": "conversacion_maquina",
            "confidence": 1.1748343240469694e-05
        }
    ],
    "response_selector": {
        "default": {
            "response": {
                "name": null,
                "confidence": 0.0
            },
            "ranking": [],
            "full_retrieval_intent": null
        }
    },
    "text": "He cambiado el n\u00famero de tel\u00e9fono de clave permanente y ahora no me deja entrar",
    "reply": [
        {
            "recipient_id": "id",
            "text": "La modificaci\u00f3n del n\u00famero de tel\u00e9fono asociado a Cl@ve se puede hacer de manera presencial en alguna de las oficinas de registro o v\u00eda telem\u00e1tica con DNI electr\u00f3nico o certificado digital. El acceso es el siguiente:\nhttps://www.agenciatributaria.gob.es/AEAT.sede/procedimientoini/GC27.shtml\nDebido al estado de alarma la Agencia Tributaria ha incorporado un numero tr\u00e1mite en la gesti\u00f3n de las credenciales de Cl@ve, por el que se puede modificar el n\u00famero de tel\u00e9fono asociado al usuario aportado v\u00eddeo e informaci\u00f3n relacionada. Puedes acceder a trav\u00e9s del siguiente enlace: https://www.agenciatributaria.gob.es/AEAT.sede/procedimientoini/GC27.shtml"
        }
    ]
}

import requests
PORT = "5000"
HEADERS = {'Content-Type': 'application/json'}
payload = {"sender":"id", "message": "He cambiado el número de teléfono de clave permanente y ahora no me deja entrar"}
response =  requests.post('http://127.0.0.1:' + PORT + '/talk', json=payload, headers=HEADERS)