#!/bin/bash

# Arrancar endpoint 
gunicorn --access-logfile ../logs/access.log --log-file ../logs/gunicorn.log --reload --daemon --workers 1 --threads 2 -b 0.0.0.0:9000 wsgi
