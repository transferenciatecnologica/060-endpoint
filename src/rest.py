#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, request, jsonify, session, Response
import requests
import json
import logging
import logzero
from logzero import logger

application = Flask(__name__)

# Configuración
BOT_PORT = "5000"
HEADERS = {'Content-Type': 'application/json'}
LOG_PATH = "../logs/app.log"
LOG_LEVEL = logging.DEBUG

@application.before_first_request
def start():
	logzero.logfile(LOG_PATH, maxBytes=1e7, backupCount=3)
	formatter = logging.Formatter('[%(levelname)1.1s %(asctime)s] %(message)s')
	logzero.formatter(formatter)

@application.route('/', methods=['GET', 'POST'])
def hello_world():
	return 'Enpoint de comunicación con Rasa'

@application.route('/status', methods=['GET', 'POST'])
def status():
	response = requests.get('http://127.0.0.1:' + BOT_PORT + '/status')
	logger.info("Checking status")
	forward_response = Response(response=response.content, status=response.status_code, headers=HEADERS)
	return forward_response

@application.route('/talk', methods=['POST'])
def talk():
	# Se carga el payload de la petición {"sender": "ID", "message": "message"}
	payload_core = request.get_json()

	# Comprobamos que la petición esté completa
	if not set(("sender","message")).issubset(payload_core):
		return jsonify({"mensaje": "Faltan argumentos"}), 400

	# Petición a NLU (clasificación de intent)
	payload_nlu = {"text": payload_core['message']}
	response_nlu =  requests.post('http://127.0.0.1:' + BOT_PORT + '/model/parse', json=payload_nlu, headers=HEADERS)
	status_code = response_nlu.status_code

	# Petición a Rasa (chatbot)
	response_core = requests.post('http://127.0.0.1:' + BOT_PORT + '/webhooks/rest/webhook', json=payload_core, headers=HEADERS)

	# Se loggea la interacción
	logger.info("ID: {}, intent: {}, confidence: {}".format(payload_core['sender'], response_nlu.json()['intent']["name"], response_nlu.json()['intent']["confidence"]))

	# Obtener datos comunes para la respuesta
	intent_name = response_nlu.json()["intent"]["name"]
	context = intent_name.split("_")[0]

	# Composición respuesta
	response_query = {}
	response_query.update(response_nlu.json())
	response_query["intent"].update({
		"displayName": intent_name
	})

	# Rellenamos el fulfillment
	response_fulfillment = {}
	text_array = []
	custom_payload = {}
	for item in response_core.json():
		if "text" in item:
			text_array.append(item["text"])
		if "custom" in item:
			custom_payload = item["custom"]
	fulfillment = {
		"text": {
			"text": text_array 
		},
		"outputContext": context,
		"payload": custom_payload
	}
	response_fulfillment = {"fulfillmentMessages": [fulfillment]}
	response_query.update(response_fulfillment)

	response = {"queryResult": response_query}

	forward_response = Response(response=json.dumps(response), status=status_code, headers=HEADERS)
	return forward_response

if __name__ == "__main__":
	application.run('0.0.0.0', 9000, debug=False) 
